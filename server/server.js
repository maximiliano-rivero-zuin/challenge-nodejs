// const mongoose = require('mongoose');

// const conectarDB = async() =>{
//     await mongoose.connect("mongodb+srv://cluster0.cjoy5.mongodb.net/myFirstDatabase", {
//         useNewUrlParser: true,
//         useUnifiedTopology: true,
//         useFindAndModify: false,
//         useCreateIndex: true
//     });
// }

// conectarDB()

const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);
app.use(cors ());

const port = process.env.PORT || 5000;
app.listen(port, () => {
    console.log ("El servidor se está ejecutando en el port " + port);
});

app.use('/cities', require('./routes/cities'))